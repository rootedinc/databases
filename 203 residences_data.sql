INSERT INTO residences (ResidenceID, CityID, PostalCode, StreetName, HouseNumber, Addition)
VALUES
    -- Zevenaar
       (1, 1, '6901DG', 'Tooropstraat', '2', NULL),
    -- Giesbeek
       (2, 2, '6987AD', 'Kerkstraat', '76', NULL),
    -- ''s Heerenberg
       (3, 3, '7041DP', 'Lengelseweg', '50', NULL),
    -- Didam
       (4, 4, '6941BL', 'Domela Nieuwenhuisstraat', '23', NULL),
    -- Rotterdam
       (5, 5, '3036HE', 'Zaagmolenstraat', '61', NULL),
    -- Rozenburg
       (6, 6, '3181NM', 'Verschansing', '31', NULL),
    -- Den Haag
       (7, 7, '2546TL', 'Leersumstraat', '36', NULL),
    -- Scheveningen
       (8, 8, '2582NW', 'Frederik Hendriklaan', '2', NULL),
    -- Utrecht
       (9, 9, '3521TJ', 'Van der Goesstraat', '3', NULL),
    -- De Meern
       (10, 10, '3454EJ', 'Ten Veldestraat', '41', NULL),
    -- Eindhoven
       (11, 11, '5624AT', 'De Bazelstraat', '18', NULL),
    -- Acht
       (12, 12, '5626BE', 'Scheldestraat', '9', NULL);