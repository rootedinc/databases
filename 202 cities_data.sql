INSERT INTO cities (CityID, MunicipalityID, CityName, CityStatus)
VALUES
-- Gem. Zevenaar
       (1, 1, 'Zevenaar', TRUE),
       (2, 1, 'Giesbeek', FALSE),
-- Gem. Montferland
       (3, 2, '''s Heerenberg', TRUE),
       (4, 2, 'Didam', FALSE),
    -- Gem. Rotterdam
       (5, 3, 'Rotterdam', TRUE),
       (6, 3, 'Rozenburg', FALSE),
-- Gem. Den Haag
       (7, 4, 'Den Haag', TRUE),
       (8, 4, 'Scheveningen', FALSE),
-- Gem. Utrecht
       (9, 5, 'Utrecht', TRUE),
       (10, 5, 'De Meern', FALSE),
-- Gem. Eindhoven
       (11, 6, 'Eindhoven', TRUE),
       (12, 6, 'Acht', FALSE),
-- Gem. Tilburg
       (13, 7, 'Tilburg', TRUE),
       (14, 7, 'Berkel-Enschot', FALSE),
-- Gem. Flevoland
       (15, 8, 'Almere', TRUE),
       (16, 8, 'Urk', FALSE),
-- Gem. Breda
       (17, 9, 'Breda', TRUE),
       (18, 9, 'Effen', FALSE),
-- Gem. Groningen
       (19, 10, 'Groningen', TRUE),
       (20, 10, 'Dorkwerd', FALSE);
