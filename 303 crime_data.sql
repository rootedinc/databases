INSERT INTO crimes (CrimeID, RecordID, Description, Severity)
VALUES -- Hans Goor       [Computervredebreuk]
       (1, 1, 'Hacked the FBI (with CivID 5)', 5),
       (2, 1, 'Lawlessness', 3),
       -- Ticho Koerntjes [Computervredebreuk]
       (3, 2, 'Hacked the FBI (with CivID 1)', 5)