CREATE TABLE municipalities (
  MunicipalityID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  Name           VARCHAR(50)     NOT NULL,
  Inhabitants    INT             NOT NULL
);

CREATE TABLE cities (
  CityID         INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  MunicipalityID INT             NOT NULL,
  CityName       VARCHAR(50)     NOT NULL,
  CityStatus     BOOLEAN         NOT NULL DEFAULT FALSE,

  FOREIGN KEY (MunicipalityID) REFERENCES municipalities (MunicipalityID)
);

CREATE TABLE residences (
  ResidenceID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  CityID      INT             NOT NULL,
  PostalCode  VARCHAR(6)      NOT NULL,
  StreetName  VARCHAR(50)     NOT NULL,
  HouseNumber INT             NOT NULL,
  Addition    VARCHAR(5),

  FOREIGN KEY (CityID) REFERENCES cities (CityID)
);

CREATE TABLE civilians (
  CivID        INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  FullName     VARCHAR(100)    NOT NULL,
  SurName      VARCHAR(50)     NOT NULL,
  DateOfBirth  DATE            NOT NULL,
  PlaceOfBirth VARCHAR(100)    NOT NULL,
  Sex          CHAR(1)         NOT NULL,
  Nationality  VARCHAR(100)    NOT NULL,
  Height       INT             NOT NULL,
  ResidenceID  INT             NULL,
  PhotoPath    TEXT            NULL,

  FOREIGN KEY (ResidenceID) REFERENCES residences (ResidenceID)
);

CREATE TABLE criminal_record (
  RecordID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  CivID    INT             NOT NULL,
  Wanted   BOOLEAN         NOT NULL DEFAULT FALSE,

  FOREIGN KEY (CivID) REFERENCES civilians (CivID)
);

CREATE TABLE crimes (
  CrimeID     INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  RecordID    INT             NOT NULL,
  Description TEXT            NOT NULL,
  Severity    INT             NOT NULL DEFAULT 1,

  FOREIGN KEY (RecordID) REFERENCES criminal_record (RecordID)
);

