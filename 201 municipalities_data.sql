INSERT INTO municipalities (MunicipalityID, Name, Inhabitants)
VALUES (1, 'Zevenaar', 32522),
       (2, 'Montferland', 35532),
       (3, 'Rotterdam', 641326),
       (4, 'Den Haag', 534158),
       (5, 'Utrecht', 349234),
       (6, 'Eindhoven', 229637),
       (7, 'Tilburg', 215946),
       (8, 'Flevoland', 205058),
       (9, 'Breda', 183851),
       (10, 'Groningen', 203954);