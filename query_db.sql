-- Query a civilian's name with CivID 1.
SELECT FullName, SurName FROM civilians WHERE CivID = 1;

-- Query all males in the database.
SELECT CivID, FullName, SurName FROM civilians WHERE Sex = 'M' ORDER BY DateOfBirth;

-- Query if a Civilian has a criminal record. (No rows if no record)
SELECT RecordID FROM criminal_record WHERE CivID = 1; -- Has record    (1 row )
SELECT RecordID FROM criminal_record WHERE CivID = 2; -- Has no record (0 rows)

-- Query all cities with city status.
SELECT CityName FROM cities WHERE CityStatus = TRUE;

-- Query a picture path of a civilian (if available == NOT NULL)
SELECT CivID, FullName, SurName, PhotoPath FROM civilians WHERE CivID = 1;

-- -----------------------------------------------------------

-- Query all residences in a city with CityID 1.
SELECT c.CityName, residences.ResidenceID, residences.PostalCode, residences.StreetName, residences.HouseNumber, residences.Addition
FROM residences INNER JOIN cities c ON residences.CityID = c.CityID
WHERE c.CityID = 1;

-- Query all civilians with a criminal record.
SELECT civilians.CivID, civilians.FullName, civilians.SurName, cr.RecordID, cr.Wanted
FROM civilians INNER JOIN criminal_record cr ON civilians.CivID = cr.CivID;

-- Query all crimes a civilian (with a criminal record of ID 1 and CivID 1) has committed.
SELECT criminal_record.RecordID, criminal_record.CivID, c.Description, c.Severity
FROM criminal_record INNER JOIN crimes c ON criminal_record.RecordID = c.RecordID
WHERE c.RecordID = 1;

-- Query all civilians in residences and group them together.
SELECT residences.ResidenceID, c.CivID, c.FullName, c.SurName
FROM residences INNER JOIN civilians c ON residences.ResidenceID = c.ResidenceID
ORDER BY c.ResidenceID;

-- -----------------------------------------------------------

-- Query all crimes a civilian has committed and group them by name.
SELECT civilians.CivID, civilians.FullName, civilians.SurName, cr.RecordID, c.Description, c.Severity
FROM ((civilians
    INNER JOIN criminal_record cr on civilians.CivID = cr.CivID)
    INNER JOIN crimes c on c.RecordID = cr.RecordID)
ORDER BY civilians.SurName;

-- Query all civilians in a city (with CityID 1) and group them by residence.
SELECT civilians.CivID, civilians.FullName, civilians.SurName, r.ResidenceID, r.StreetName, c.CityID, c.CityName
FROM ((civilians
    INNER JOIN residences r on civilians.ResidenceID = r.ResidenceID)
    INNER JOIN cities c on r.CityID = c.CityID)
WHERE c.CityID = 1
ORDER BY r.ResidenceID;

-- -----------------------------------------------------------

-- Query all crimes civilians have committed in a city, and get their address.
SELECT civilians.CivID, civilians.FullName, civilians.SurName, r.ResidenceID, r.StreetName, r.HouseNumber, ct.CityID, ct.CityName, cr.RecordID, c.Description, c.Severity
FROM ((((civilians
    INNER JOIN residences r on civilians.ResidenceID = r.ResidenceID)
    INNER JOIN criminal_record cr on civilians.CivID = cr.CivID)
    INNER JOIN crimes c ON cr.RecordID = c.RecordID)
    INNER JOIN cities ct ON r.CityID = ct.CityID)
WHERE ct.CityID = 2
ORDER BY civilians.SurName;

-- Query all crimes civilians have committed in a municipality, and get their address.
SELECT civilians.CivID, civilians.FullName, civilians.SurName, m.MunicipalityID, m.Name, r.ResidenceID, r.StreetName, r.HouseNumber, ct.CityID, ct.CityName, cr.RecordID, c.Description, c.Severity
FROM (((((civilians
    INNER JOIN residences r on civilians.ResidenceID = r.ResidenceID)
    INNER JOIN criminal_record cr on civilians.CivID = cr.CivID)
    INNER JOIN crimes c on cr.RecordID = c.RecordID)
    INNER JOIN cities ct on r.CityID = ct.CityID)
    INNER JOIN municipalities m on ct.MunicipalityID = m.MunicipalityID)
WHERE m.MunicipalityID = 1
ORDER BY civilians.SurName;