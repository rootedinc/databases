INSERT INTO civilians (CivID, FullName, SurName, DateOfBirth, PlaceOfBirth, Sex, Nationality, Height, ResidenceID, PhotoPath)
VALUES
       -- Fam. Goor
       (1, 'Hans', 'Goor', '2001-07-12', 'Arnhem', 'M', 'Netherlands', 187, 2,
           'civilians_photos/1.png'),
       (2, 'Sam', 'Goor', '2002-12-19', 'Arnhem', 'M', 'Netherlands', 192, 2, NULL),
       (3, 'Leen', 'Goor', '1957-10-22', 'Rotterdam', 'M', 'Netherlands', 180, 2, NULL),
       (4, 'Elsa', 'Goor', '1968-04-23', 'Arnhem', 'V', 'Netherlands', 184, 2, NULL),

       -- Fam. Koerntjes
       (5, 'Ticho', 'Koerntjes', '2001-02-04', 'Zevenaar', 'M', 'Netherlands', 187, 1, NULL),
       (6, 'Dion', 'Koerntjes', '2000-02-01', 'Zevenaar', 'M', 'Netherlands', 190, 1, NULL),
       (7, 'Harold', 'Koerntjes', '1960-01-14', 'Zevenaar', 'M', 'Netherlands', 190, 1, NULL),
       (8, 'Susan', 'Koerntjes', '1960-02-20', 'Zevenaar', 'V', 'Netherlands', 173, 1, NULL),

       -- Nick Kuppens
       (9, 'Nick', 'Kuppens', '2000-03-14', 'Didam', 'M', 'Netherlands', 189, 4,
           'civilians_photos/9.png'),
       (10, 'Wolf', 'Kuppens', '2011-04-09', 'Didam', 'M', 'Netherlands', 92, 4,
           'civilians_photos/10.png');